<readingList if={show}>
  <div class="recommended">
    <blockquote class="featuredQuote"><a target="_blank" href="http://pinterest.com/pin/96475616990198292/">"Always code as if the guy who ends up maintaining, or testing your code will be a violent psychopath who knows where you live."</a></blockquote>
    <div class="listItems">

      <div class="listItem first"><a target="_blank" href="https://twitter.com/brandon_m_lyon">My Twitter account</a></div>

      <div class="listItem"><a target="_blank" href="http://www.lizengland.com/blog/2014/04/the-door-problem/">The door problem</a></div>

      <div class="listItem"><a target="_blank" href="https://www.sempf.net/post/On-Testing1">A software tester walks into a bar</a></div>

      <div class="listItem"><a target="_blank" href="https://lawsofux.com/">The Laws of UX</a></div>

      <div class="listItem"><a target="_blank" href="http://www.csse.monash.edu.au/~damian/papers/HTML/Plurals.html">Pluralizing English strings</a></div>

      <div class="listItem"><a target="_blank" href="https://medium.com/ux-power-tools/100-questions-designers-always-ask-8b9f441bcd35">100 important questions</a></div>

      <div class="listItem"><a target="_blank" href="https://www.youtube.com/watch?v=jVL4st0blGU">Depth vs complexity</a></div>

      <div class="listItem"><a target="_blank" href="https://medium.com/backchannel/immersive-design-76499204d5f6#.gkalrqgrd">How to design for virtual reality</a></div>

      <div class="listItem"><a target="_blank" href="http://khan.github.io/tota11y/">An accessibility visualization toolkit</a></div>


      <div class="listItem"><a target="_blank" href="http://www.funnyant.com/choosing-javascript-mvc-framework/">Choosing a javascript MVC framework</a></div>

      <div class="listItem"><a target="_blank" href="https://www.youtube.com/watch?v=SmE4OwHztCc">How does a browser render a website?</a></div>

      <div class="listItem"><a target="_blank" href="http://uptodate.frontendrescue.org/">How to keep up to date on frontend technologies</a></div>

      <div class="listItem"><a target="_blank" href="http://blog.anguscroll.com/if-kerouac-wrote-javascript">If Kerouac wrote JavaScript</a></div>

      <div class="listItem"><a target="_blank" href="http://csshumor.com/">CSS Humor</a></div>

      <div class="listItem"><a target="_blank" href="http://blog.kissmetrics.com/100-conversion-optimization-case-studies/">100 conversion optimization studies</a></div>

      <div class="listItem"><a target="_blank" href="http://searchengineland.com/seotable">The SEO periodic table</a></div>

      <div class="listItem"><a target="_blank" href="http://userium.com/">The usability checklist</a></div>

      <div class="listItem"><a target="_blank" href="https://www.iubenda.com/en">Privacy policy generator</a></div>

      <div class="listItem"><a target="_blank" href="http://www.youtube.com/watch?feature=player_embedded&v=D4HbWhri7Tc">50 quick photography tips</a></div>

      <div class="listItem"><a target="_blank" href="http://boagworld.com/working-in-web/john/">Is the client stupid or are you failing him</a></div>

      <div class="listItem"><a target="_blank" href="http://sethgodin.typepad.com/seths_blog/2005/09/free_ebook_1_no.html">Seth Godin's guide to making a website that works</a></div>

      <div class="listItem"><a target="_blank" href="http://alistapart.com/article/dao">A dao of web design</a></div>

      <div class="listItem"><a target="_blank" href="http://www.smashingmagazine.com/2012/04/24/a-closer-look-at-font-rendering/">A closer look at font rendering</a></div>

      <div class="listItem"><a target="_blank" href="http://lifehacker.com/5967464/treat-your-freelance-work-like-a-stock-portfolio-to-meet-your-income-goals">Treat your design portfolio like a stock portfolio</a></div>

      <div class="listItem"><a target="_blank" href="http://purecss.io/forms/">Styling forms</a></div>

      <div class="listItem"><a target="_blank" href="http://snook.ca/archives/html_and_css/font-size-with-rem">Font size with rem</a></div>

      <div class="listItem"><a target="_blank" href="http://coding.smashingmagazine.com/2012/10/04/the-code-side-of-color/">The code side of color</a></div>

      <div class="listItem"><a target="_blank" href="http://goodui.org/">A good UI</a></div>

      <div class="listItem"><a target="_blank" href="http://blog.kissmetrics.com/seduce-your-web-visitors/">50 ways to seduce your website visitors</a></div>

      <div class="listItem"><a target="_blank" href="http://www.worqx.com/color/itten.htm">Itten's color contrasts</a></div>

      <div class="listItem"><a target="_blank" href="http://bjk5.com/post/44698559168/breaking-down-amazons-mega-dropdown">Amazon's mega dropdown menu</a></div>

    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/read-me/', function () {
    self.show = true;
    self.update();
    //document.documentElement.style.setProperty('--main-color', '#793bdb');/*purple*/
  });
  </script>
</readingList>
