<projects-watches if={show}>
  <div class="projects">
    <nav-projects />
    <div id="subcontentMain">
      <div class="watches">
        <h3>Samsung Gear S2</h3>
        <hr/>
        <div class="watch">
          <img src="../images/watches/dnu01v001.png" />
          <p>BLW Classy Digital</p>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/projects/watches/', function () {
    self.show = true;
    self.update();
  });
  </script>
</projects-watches>
