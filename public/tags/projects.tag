<projects if={show}>
  <div class="projects">
    <nav-projects />
    <div id="subcontentMain">
      <div class="moodboards">
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/website-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/mobile-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/graphic-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/colorways/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/packaging-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/photographic-inspiration/"></a>
        </div>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/projects/', function () {
    self.show = true;
    self.update();
    var showPins = function() {
      var script = document.createElement('script');
      var prior = document.getElementsByTagName('script')[0];
      script.async = 1;
      prior.parentNode.insertBefore(script, prior);
      script.src = 'https://assets.pinterest.com/js/pinit_main.js';
    };
    showPins();
    //document.documentElement.style.setProperty('--main-color', '#58df08');/*green*/
  });
  </script>
</projects>
