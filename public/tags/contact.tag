<contact if={show}>
  <div class="contact">
    <ul>
      <li>Brandon Lyon</li>
      <li>805-689-0077</li>
      <li><a href='mailt&#111;&#58;%62m%6&#67;y%6Fn&#64;g%6&#68;%61&#105;l&#46;%63%6Fm'>&#98;m&#108;yon&#64;&#103;m&#97;&#105;l&#46;&#99;&#111;m</a></li>
      <li><a href="http://www.linkedin.com/pub/brandon-lyon/5/aa4/6a">LinkedIn</a></li>
      <li><a href="https://twitter.com/brandon_m_lyon">Twitter</a></li>
      <li><a href="/bmlyon_resume.pdf">Resume</a></li>
    </ul>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/contact/', function () {
    self.show = true;
    self.update();
    //document.documentElement.style.setProperty('--main-color', '#eb702b');/*orange*/
  });
  </script>
</contact>
