<projects-websites if={show}>
  <div class="projects">
    <nav-projects />
    <div id="subcontentMain">
      <div class="websites">
        <div class="listItems" style="max-width: 450px;">
          <div class="listItem">
            <img src="../images/w_c_ee10.jpg" />
            <ul>
              <li>Millions of registered members</li>
              <li>Subscription based site</li>
              <li>Over 150,000 active lines of CSS</li>
              <li>Tons of complex javascript</li>
              <li>Hundreds of different page types with multiple rotations each</li>
              <li>Supports 12+ different user-generated content types</li>
              <li>Unique SEO challenges</li>
              <li>Custom-built CMS</li>
            </ul>
          </div>
          <div class="listItem">
            <img src="../images/w_c_mns1.jpg" />
              <ul>
                <li>Built with components based upon design patterns</li>
                <li>Bespoke flat file based CMS</li>
                <li>Integrated with Wordpress</li>
                <li>Custom VPS server setup</li>
              </ul>
          </div>
          <div class="listItem">
            <img src="../images/w_c_sdfcnew.jpg" />
            <ul>
              <li>Maintained and replaced legacy undocumented e-commerce system</li>
              <li>Created event and social media portals using Wordpress and other tools</li>
              <li>Provided product and event photography for use on the website</li>
            </ul>
          </div>
          <div class="listItem">
            <img src="../images/w_c_x2.jpg" />
            <ul>
              <li>Responsive redesign of an existing site</li>
              <li>Large focus on clarified &amp; succinct content &amp; navigation</li>
              <li>Clear CTAs (calls to action) presented throughout the design</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/projects/websites/', function () {
    self.show = true;
    self.update();
  });
  </script>
</projects-websites>
