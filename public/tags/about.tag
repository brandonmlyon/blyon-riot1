<about if={show}>
  <div class="about">
    <nav-about />
    <div id="subcontentMain">
      <div class="aboutMe">
        <blockquote>Design is everywhere. Look at the computing device in front of you, your car, and your clothes. Someone designed those things.</blockquote>
        <p>As a person with an eye for detail the design profession seemed like a perfect match for me from an early age. I love to research, plan, and create. I can't imagine doing anything else with my life. I enjoy discussing modern design trends, learning new techniques, and figuring out how things are built and work. Reading design and technology news on a daily basis is an important part of my life.</p>
        <p>Every weekend I bake a new variety of cookie and share the experiment with my coworkers. Baking is a kind of science with experiments and tasty metrics!</p>
        <p>If I had to pick a few of my favorite things other than a computer, they would be:</p>
        <ul>
          <li>CMYK</li>
          <li>Frisbees</li>
          <li>Volleyball</li>
          <li>Cars</li>
          <li>Bicycles</li>
          <li>Music</li>
        </ul>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/about/', function () {
    self.show = true;
    self.update();
    //document.documentElement.style.setProperty('--main-color', '#00a8e8');/*cyan*/
  });
  </script>
</about>
