<about-myDesigns if={show}>
  <div class="about">
    <nav-about />
    <div id="subcontentMain">
      <div class="aboutMyDesigns">
        <p>All good design comes from proper planning. Understanding a project's constraints leads to cost effective, feasible, and sustainable designs. Using a one-size-fits-all approach isn't just inefficient, its detrimental. I start by analyzing your project needs and coming up with a few good options for how to proceed. Most clients want to maximize their website's ROI (return on investment) and lower their maintenance costs.</p>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/about/my-designs/', function () {
    self.show = true;
    self.update();
  });
  </script>
</about-myDesigns>
