<app>
  <home />
  <about />
    <about-myDesigns />
    <about-thisWebsite />
    <about-testimonials />
  <projects />
    <projects-websites />
    <projects-watches />
  <contact />
  <readingList />
  <show-app />
  <script>
    this.on('mount', function() {
      riot.route.start(true);
    });
  </script>
</app>
