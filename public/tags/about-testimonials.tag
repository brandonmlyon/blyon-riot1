<about-testimonials if={show}>
  <div class="about">
    <nav-about />
    <div id="subcontentMain">
      <div class="testimonials">
        <blockquote class="featuredQuote">"He's always thoughtful and mindful in his implementations. We know Brandon will produce very high quality work."</blockquote>
        <ul class="quoteList">
          <li>"I wish he had time to work on all of my projects."</li>
          <li>"Brandon seems to really know his stuff when it comes to design. When a problem arises he usually provides a range of options for how to resolve it and is ready with pros and cons of each."</li>
          <li>"He is diligent on his tasks, considerate to other teams, and tries to follow up on all issues. He doesn't like to pass the buck to someone else."</li>
          <li>"Brandon has earned the title honorary QA for all of the bugs he has uncovered while taking the time to figure out each project's inner workings."</li>
          <li>"Brandon asks all of the right questions about expected functionality."</li>
          <li>"He keeps abreast of changes that are going on in the tech world and discusses them with his coworkers."</li>
          <li>"He produces high quality content and has excellent attention to detail."</li>
          <li>"Despite extremely busy schedules Brandon still manages to produce high quality work."</li>
          <li>"One of our most dependable producers."</li>
        </ul>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/about/testimonials/', function () {
    self.show = true;
    self.update();
  });
  </script>
</about-testimonials>
