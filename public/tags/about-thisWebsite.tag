<about-thisWebsite if={show}>
  <div class="about">
    <nav-about />
    <div id="subcontentMain">
      <div class="aboutThisWebsite">
        <p>This website was created by hand in very little time using minimalist theories. The project was once a VPS hosted Meteor app. Next I turned it into a git hosted Polymer project. Now it is a Firebase deployed Riot.js app. I plan to rebuild it using svelte.js, possibly on Netlify.</p>
      </div>
    </div>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  });
  localRoute('/about/this-website/', function () {
    self.show = true;
    self.update();
  });
  </script>
</about-thisWebsite>
