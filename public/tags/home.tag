<home if={show}>
  <div class="homepage">
    <p>Welcome! I've been a professional frontend developer for over 15 years. I've created websites, mobile apps, wearables, mixed reality VR/AR, voice assistants, and IoT devices.</p>
    
    <p>Frontend is my specialty but I've done full stack development including devops and a little bit of soldering. Websites are my passion but I also design physical things for multiple industries. Graphic design, apparel design<strong style="color:#e41b5b;">*</strong>, industrial design, architectural design, and photography.</p>
    
    <p>I also UX! Several of my recent projects were design prototypes for data-driven user-centered projects revamping core features of legacy products with large userbases.</p>

    <p><sub><strong style="color:#e41b5b;">*</strong><em>when I say "apparel design" I don't mean sticking a graphic on a shirt (though I've done that too). I mean tooling and import taxes on artificial textiles kind of stuff with multi-year color forecasting based on market trends.</em></sub></p>
  </div>
  <script>
  var self = this;
  var localRoute = riot.route.create();
  localRoute(function () {
    self.show = false;
    self.update();
  })
  localRoute('/', function () {
    self.show = true;
    self.update();
    //document.documentElement.style.setProperty('--main-color', '#e41b5b');/*magenta*/
  });
  </script>
</home>
