

/*
    Left off at making global variable for domain for url structure

    app needs animations

*/

# (home icon)

  Welcome! Thank you for your interest in Brandon M Lyon and my services. This app will help you get to know me and teach you a bit about software development.

# (person icon)

  (facebook devices photo)

  I have over 18 years of experience crafting websites. I love to research, plan, and create. I enjoy discussing modern design trends, learning new techniques, and figuring out how things are built and work. Every weekend I bake a new variety of cookie and share the experiment with my coworkers. Baking is a kind of science with experiments and tasty metrics!

  Frontend development experience:
  * Responsive and adaptive websites
  * Mobile apps
  * Desktop apps
  * Wearables
  * Voice interaction
  * AR

  Milestones:
  * Managed frontend code for a subscription website and app with millions of users and >150,000 lines of CSS
  * Headed a graphic design department and did full-stack development for an e-commerce website
  * Produced research books and websites for multimillion-dollar projects
  * Was responsible for legally binding construction documents
  * Designed apparel for final manufacture

  Other skills: {{design as tags}}:
  * UX & usability testing
  * Isomorphic app frameworks
  * Metrics, metrics, metrics
  * Performance testing
  * Hardware
  * Devops
  * SEO

# (computer icon)

  This app was built and published in an afternoon using Jasonette. I was able to develop it quickly because of experience I gathered while building other native apps. I wrote an article in case you you would like to learn about https://www.experts-exchange.com/articles/28608/Mobile-App-Development-Best-Practices.html mobile app development best practices.

# (book icon)

  Recommended reading links

# (news icon)

  (my tweets)

# (paint brush and palette icon)

  (my pins)
